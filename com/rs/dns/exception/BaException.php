<?php
namespace com\rs\dns\exception;

use restphp\exception\RestException;

class BaException extends RestException {
    public function __construct($strBaCode, $intHttpStatus = 400) {
        parent::__construct("[" . $strBaCode . "]", APP_NAME . "/" . $strBaCode, $intHttpStatus);
    }
}
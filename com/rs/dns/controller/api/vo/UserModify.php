<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class UserModify
 * @package com\controller\api\vo
 */
final class UserModify {
    /**
     * @var string 密码.
     */
    private $_password;

    /**
     * @var string 真实姓名.
     * @length(min=1, max=20, message=真实姓名不能为空)
     */
    private $_realname;

    /**
     * @var string 手机号码.
     * @mobile
     */
    private $_tel;

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->_password = $password;
    }

    /**
     * @return string
     */
    public function getRealname()
    {
        return $this->_realname;
    }

    /**
     * @param string $realName
     */
    public function setRealname($realName)
    {
        $this->_realname = $realName;
    }

    /**
     * @return string
     */
    public function getTel()
    {
        return $this->_tel;
    }

    /**
     * @param string $phone
     */
    public function setTel($phone)
    {
        $this->_tel = $phone;
    }
}
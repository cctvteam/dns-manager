<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class Ipv4BatchForm
 * @package com\rs\dns\controller\api\vo
 */
final class Ipv4BatchForm {
    /**
     * @var string 网络组ID.
     * @customer(method="\com\rs\dns\controller\api\validate\ValidateNetId::exist", message="网络组信息不正确")
     */
    private $_netid;

    /**
     * @var string ip地址信息.
     * @customer(method="\com\rs\dns\controller\api\validate\ValidateIpv4::batchTxt")
     */
    private $_ipTxt;

    /**
     * @return string
     */
    public function getNetid()
    {
        return $this->_netid;
    }

    /**
     * @param string $netid
     */
    public function setNetid($netid)
    {
        $this->_netid = $netid;
    }

    /**
     * @return string
     */
    public function getIpTxt()
    {
        return $this->_ipTxt;
    }

    /**
     * @param string $ipTxt
     */
    public function setIpTxt($ipTxt)
    {
        $this->_ipTxt = $ipTxt;
    }
}
<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class HostBatchForm
 * @package com\rs\dns\controller\api\vo
 */
final class HostBatchForm {
    /**
     * @var string ip地址信息.
     * @customer(method="\com\rs\dns\controller\api\validate\ValidateHost::batchTxt")
     */
    private $_ipTxt;

    /**
     * @return string
     */
    public function getIpTxt()
    {
        return $this->_ipTxt;
    }

    /**
     * @param string $ipTxt
     */
    public function setIpTxt($ipTxt)
    {
        $this->_ipTxt = $ipTxt;
    }
}
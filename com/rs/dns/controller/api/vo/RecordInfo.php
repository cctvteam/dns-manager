<?php
namespace com\rs\dns\controller\api\vo;

final class RecordInfo {
    /**
     * @var string 记录ID.
     */
    private $_id;

    /**
     * @var string 域名.
     */
    private $_origin;

    /**
     * @var integer 域名ID.
     */
    private $_zone;

    /**
     * @var string 所属用户账号.
     */
    private $_username;

    /**
     * @var string 主机头名称.
     */
    private $_name;

    /**
     * @var string 解析类型.
     */
    private $_type;

    /**
     * @var integer 网络组（解析线路）ID.
     */
    private $_netid;

    /**
     * @var string 网络组（解析线路）名称.
     */
    private $_netname;

    /**
     * @var string 解析值.
     */
    private $_data;

    /**
     * @var integer 优先级.
     */
    private $_aux;

    /**
     * @var integer ttl值.
     */
    private $_ttl;

    /**
     * @var integer 负载均衡.
     */
    private $_balance;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->_origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin($origin)
    {
        $this->_origin = $origin;
    }

    /**
     * @return int
     */
    public function getZone()
    {
        return $this->_zone;
    }

    /**
     * @param int $zone
     */
    public function setZone($zone)
    {
        $this->_zone = $zone;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->_type = $type;
    }

    /**
     * @return int
     */
    public function getNetid()
    {
        return $this->_netid;
    }

    /**
     * @param int $netid
     */
    public function setNetid($netid)
    {
        $this->_netid = $netid;
    }

    /**
     * @return string
     */
    public function getNetname()
    {
        return $this->_netname;
    }

    /**
     * @param string $netname
     */
    public function setNetname($netname)
    {
        $this->_netname = $netname;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->_data = $data;
    }

    /**
     * @return int
     */
    public function getAux()
    {
        return $this->_aux;
    }

    /**
     * @param int $aux
     */
    public function setAux($aux)
    {
        $this->_aux = $aux;
    }

    /**
     * @return int
     */
    public function getTtl()
    {
        return $this->_ttl;
    }

    /**
     * @param int $ttl
     */
    public function setTtl($ttl)
    {
        $this->_ttl = $ttl;
    }

    /**
     * @return int
     */
    public function getBalance()
    {
        return $this->_balance;
    }

    /**
     * @param int $balance
     */
    public function setBalance($balance)
    {
        $this->_balance = $balance;
    }
}
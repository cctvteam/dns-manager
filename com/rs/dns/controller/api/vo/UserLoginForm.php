<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class UserLoginForm
 * @package com\controller\api\vo
 */
final class UserLoginForm {
    /**
     * @var string 登录账号.
     * @length(min=1,max=20,message=登录账号错误)
     */
    private $_account;

    /**
     * @var string 账号密码.
     * @notnull(message=账号密码不能为空)
     */
    private $_pwd;

    /**
     * @var string 验证码.
     * @length(min=4,max=4,message=验证码错误)
     */
    private $_randCode;

    /**
     * =================== getter and setter =====================
     */

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->_account;
    }

    /**
     * @param string $account
     */
    public function setAccount($account)
    {
        $this->_account = $account;
    }

    /**
     * @return string
     */
    public function getPwd()
    {
        return $this->_pwd;
    }

    /**
     * @param string $pwd
     */
    public function setPwd($pwd)
    {
        $this->_pwd = $pwd;
    }

    /**
     * @return string
     */
    public function getRandCode()
    {
        return $this->_randCode;
    }

    /**
     * @param string $randCode
     */
    public function setRandCode($randCode)
    {
        $this->_randCode = $randCode;
    }

    /**
     * =================== getter and setter =====================
     */
}
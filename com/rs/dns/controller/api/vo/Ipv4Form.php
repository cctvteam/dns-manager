<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class Ipv4Form
 * @package com\rs\dns\controller\api\vo
 */
final class Ipv4Form {
    /**
     * @var int 网络组ID.
     * @customer(method='\com\rs\dns\controller\api\validate\ValidateNetId::exist', message='网络组不存在')
     */
    private $_netid;
    /**
     * @var string IPv4地址.
     * @ipv4
     */
    private $_ip;
    /**
     * @var string 子网掩码
     * @range(min=0,max=32)
     */
    private $_mask;

    /**
     * @var string 备注.
     */
    private $_remark;

    /**
     * @return int
     */
    public function getNetid()
    {
        return $this->_netid;
    }

    /**
     * @param int $netid
     */
    public function setNetid($netid)
    {
        $this->_netid = $netid;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->_ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->_ip = $ip;
    }

    /**
     * @return string
     */
    public function getMask()
    {
        return $this->_mask;
    }

    /**
     * @param string $mask
     */
    public function setMask($mask)
    {
        $this->_mask = $mask;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->_remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark($remark)
    {
        $this->_remark = $remark;
    }
}
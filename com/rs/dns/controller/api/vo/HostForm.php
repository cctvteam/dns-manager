<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class HostForm
 * @package com\rs\dns\controller\api\vo
 */
final class HostForm {
    /**
     * @var string ip地址.
     * @ipv4
     */
    private $_IP;

    /**
     * @var string 端口
     * @range(min=0)
     */
    private $_port;

    /**
     * @return string
     */
    public function getIP()
    {
        return $this->_IP;
    }

    /**
     * @param string $IP
     */
    public function setIP($IP)
    {
        $this->_IP = $IP;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->_port;
    }

    /**
     * @param string $port
     */
    public function setPort($port)
    {
        $this->_port = $port;
    }
}
<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class UrlForm
 * @package com\rs\dns\controller\api\vo
 */
final class UrlForm {
    /**
     * @var string 域名.
     * @customer(method="\com\rs\dns\controller\api\validate\ValidateDomain::exist",message=“域名%s不存在”)
     */
    private $_origin;

    /**
     * @var string 主机名.
     */
    private $_name;

    /**
     * @var string 类型.
     * @inArray(value=[0|1],message="转发类型值不正确")
     */
    private $_URLtype;

    /**
     * @var string 网络组ID.
     * @range(min=0)
     */
    private $_netid;

    /**
     * @var string 值.
     * @notnull(message="记录值不能为空")
     */
    private $_URL;

    /**
     * @var string 标题.
     * @length(max=25,message='标题最多不能超过25个字符')
     */
    private $_title;

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->_origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin($origin)
    {
        $this->_origin = $origin;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getURLtype()
    {
        return $this->_URLtype;
    }

    /**
     * @param string $URLtype
     */
    public function setURLtype($URLtype)
    {
        $this->_URLtype = $URLtype;
    }

    /**
     * @return string
     */
    public function getNetid()
    {
        return $this->_netid;
    }

    /**
     * @param string $netid
     */
    public function setNetid($netid)
    {
        $this->_netid = $netid;
    }

    /**
     * @return string
     */
    public function getURL()
    {
        return $this->_URL;
    }

    /**
     * @param string $URL
     */
    public function setURL($URL)
    {
        $this->_URL = $URL;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->_title = $title;
    }


}
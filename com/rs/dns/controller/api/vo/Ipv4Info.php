<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class Ipv4Info
 * @package com\rs\dns\controller\api\vo
 */
final class Ipv4Info {
    /**
     * @var integer 行号
     */
    private $_RowNumber;

    /**
     * @var integer 记录ID.
     */
    private $_id;

    /**
     * @var integer 网络组ID.
     */
    private $_netid;

    /**
     * @var string IP.
     */
    private $_ip;

    /**
     * @var integer 子网掩码.
     */
    private $_mask;

    /**
     * @var string 描述.
     */
    private $_remark;

    /**
     * @return int
     */
    public function getRowNumber()
    {
        return $this->_RowNumber;
    }

    /**
     * @param int $RowNumber
     */
    public function setRowNumber($RowNumber)
    {
        $this->_RowNumber = $RowNumber;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return int
     */
    public function getNetid()
    {
        return $this->_netid;
    }

    /**
     * @param int $netid
     */
    public function setNetid($netid)
    {
        $this->_netid = $netid;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->_ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->_ip = $ip;
    }

    /**
     * @return int
     */
    public function getMask()
    {
        return $this->_mask;
    }

    /**
     * @param int $mask
     */
    public function setMask($mask)
    {
        $this->_mask = $mask;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->_remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark($remark)
    {
        $this->_remark = $remark;
    }
}
<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class RecordBalanceForm
 * @package com\rs\dns\controller\api\vo
 */
final class RecordBalanceForm {
    /**
     * @var int 负载均衡模式.
     * @inArray(value=[0|1|2)
     */
    private $_balance;

    /**
     * @var array 优先级设置.
     * @customer(method="\com\rs\dns\controller\api\validate\ValidateRecordAux::check")
     */
    private $_auxList;

    /**
     * @return int
     */
    public function getBalance()
    {
        return $this->_balance;
    }

    /**
     * @param int $balance
     */
    public function setBalance($balance)
    {
        $this->_balance = $balance;
    }

    /**
     * @return array
     */
    public function getAuxList()
    {
        return $this->_auxList;
    }

    /**
     * @param array $auxList
     */
    public function setAuxList($auxList)
    {
        $this->_auxList = $auxList;
    }
}
<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class UserManage
 * @package com\controller\api\vo
 */
final class UserManage {
    /**
     * @var string 会员账号.
     * @length(min=4,max=20,message=会员账号长度为4-20个字符)
     */
    private $_username;

    /**
     * @var string 登录密码.
     */
    private $_password;

    /**
     * @var string 用户权限.
     * @inArray(value=['N'|'Y'],message='请选择用户权限')
     */
    private $_isadmin;

    /**
     * @var string 真实姓名.
     * @length(min=1,max=20,message=真实姓名长度为1-20个字符)
     */
    private $_realname;

    /**
     * @var string 手机号码.
     * @notnull(message=请输入手机号码)
     */
    private $_tel;

    /**
     * @var string 状态.
     * @inArray(value=['N'|'Y'|'ZS'|'ZR'],message=状态取值范围不正确)
     */
    private $_verify;

    /**
     * @var int 最大域名数.
     * @range(min=0,message=最大域名数不能为负数)
     */
    private $_soanum;

    /**
     * @var int 每个域名最大解析记录数.
     * @range(min=0,message=域名最大解析记录数不能为负数)
     */
    private $_rrnum;

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->_password = $password;
    }

    /**
     * @return string
     */
    public function getIsadmin()
    {
        return $this->_isadmin;
    }

    /**
     * @param string $isadmin
     */
    public function setIsadmin($isadmin)
    {
        $this->_isadmin = $isadmin;
    }

    /**
     * @return string
     */
    public function getRealname()
    {
        return $this->_realname;
    }

    /**
     * @param string $realname
     */
    public function setRealname($realname)
    {
        $this->_realname = $realname;
    }

    /**
     * @return string
     */
    public function getTel()
    {
        return $this->_tel;
    }

    /**
     * @param string $tel
     */
    public function setTel($tel)
    {
        $this->_tel = $tel;
    }

    /**
     * @return string
     */
    public function getVerify()
    {
        return $this->_verify;
    }

    /**
     * @param string $verify
     */
    public function setVerify($verify)
    {
        $this->_verify = $verify;
    }

    /**
     * @return int
     */
    public function getSoanum()
    {
        return $this->_soanum;
    }

    /**
     * @param int $soanum
     */
    public function setSoanum($soanum)
    {
        $this->_soanum = $soanum;
    }

    /**
     * @return int
     */
    public function getRrnum()
    {
        return $this->_rrnum;
    }

    /**
     * @param int $rrnum
     */
    public function setRrnum($rrnum)
    {
        $this->_rrnum = $rrnum;
    }

}
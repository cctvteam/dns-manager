<?php
namespace com\rs\dns\controller\api\vo;

/**
 * Class DomainQuery.
 * @package com\rs\dns\controller\api\vo
 */
final class DomainQuery {
    /**
     * @var string 域名.
     */
    private $_domain;
    /**
     * @var string 用户名.
     */
    private $_username;
    /**
     * @var string 状态.
     */
    private $_yn;

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->_domain;
    }

    /**
     * @param string $domain
     */
    public function setDomain($domain)
    {
        $this->_domain = $domain;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }

    /**
     * @return string
     */
    public function getYn()
    {
        return $this->_yn;
    }

    /**
     * @param string $yn
     */
    public function setYn($yn)
    {
        $this->_yn = $yn;
    }
}
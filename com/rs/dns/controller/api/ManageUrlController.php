<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\constant\CommonConstant;
use com\rs\dns\controller\api\vo\RecordForm;
use com\rs\dns\controller\api\vo\RecordQuery;
use com\rs\dns\controller\api\vo\UrlForm;
use com\rs\dns\controller\BaseController;
use com\rs\dns\exception\BaException;
use com\rs\dns\filter\ApiFilter;
use com\rs\dns\service\DomainService;
use com\rs\dns\service\UrlService;
use restphp\http\RestHttpRequest;


/**
 * Class ManageUrlController
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/manage/urls")
 */
final class ManageUrlController extends BaseController {
    /**
     * 域名管理列表.
     * @RequestMapping(value="", method="GET")
     */
    public function recordList() {
        $pageParam = RestHttpRequest::getPageParam();
        $recordQuery = RestHttpRequest::getParameterAsObject(new RecordQuery());
        $domainList = UrlService::getManageList($recordQuery, $pageParam);
        $this->_success($domainList);
    }

    /**
     * 新增域名.
     * @RequestMapping(value="", method="POST")
     */
    public function add() {
        $urlForm = RestHttpRequest::getRequestBody(new UrlForm());
        if (!DomainService::hadDomainAdmin($urlForm->getOrigin())) {
            ApiFilter::autoCreateDomain($this->_getUsername(), $urlForm->getOrigin());
        }
        RestHttpRequest::getRequestBody(new UrlForm(), true);
        UrlService::add($urlForm);
    }

    /**
     * 修改域名信息.
     * @RequestMapping(value="/{id}", method="PUT")
     * @throws BaException
     */
    public function modify() {
        $urlForm = RestHttpRequest::getRequestBody(new UrlForm(), true);
        $id = RestHttpRequest::getPathValue("id");
        UrlService::modify($id, $urlForm);
    }

    /**
     * 单个删除域名.
     * @RequestMapping(value="/{id}", method="DELETE")
     */
    public function delete() {
        $id = RestHttpRequest::getPathValue("id");
        UrlService::delete($id);
    }

    /**
     * 批量删除域名.
     * @RequestMapping(value="/actions/delete", method="POST")
     * @throws BaException
     */
    public function deleteDomainBatch() {
        $arrId = RestHttpRequest::getRequestBody();
        if (null == $arrId || empty($arrId)) {
            throw new BaException(CommonConstant::DATA_TO_DELETE_CAN_NOT_NULL);
        }
        UrlService::batchDelete($arrId);
    }

    /**
     * 旧数据关联域名信息.
     * @RequestMapping(value="/actions/old", method="POST")
     * @throws \restphp\exception\RestException
     */
    public function changeOld() {
        UrlService::changeOld();
    }
}
<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\constant\DomainConstant;
use com\rs\dns\controller\api\vo\DomainForm;
use com\rs\dns\controller\api\vo\DomainQuery;
use com\rs\dns\controller\api\vo\DomainStateSet;
use com\rs\dns\controller\api\vo\UserDomainForm;
use com\rs\dns\controller\BaseController;
use com\rs\dns\exception\BaException;
use com\rs\dns\service\DomainService;
use restphp\http\RestHttpRequest;
use restphp\utils\RestClassUtils;


/**
 * Class UserDomainController
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/user/domains")
 */
final class UserDomainController extends BaseController {
    /**
     * 域名管理列表.
     * @RequestMapping(value="", method="GET")
     */
    public function domainList() {
        $pageParam = RestHttpRequest::getPageParam();
        $domainQuery = RestHttpRequest::getParameterAsObject(new DomainQuery());
        $domainList = DomainService::getDomainList($domainQuery, $pageParam, $this->_getUsername());
        $this->_success($domainList);
    }

    /**
     * 新增域名.
     * @RequestMapping(value="", method="POST")
     * @throws BaException
     */
    public function addDomain() {
        $userDomainForm = RestHttpRequest::getRequestBody(new UserDomainForm(), true);
        $domainForm = RestClassUtils::copyFromArr(new DomainForm(), RestClassUtils::beanToArr($userDomainForm));
        $domainForm->setUsername($this->_getUsername());
        DomainService::add($domainForm);
    }

    /**
     * 修改域名信息.
     * @RequestMapping(value="/{id}", method="PUT")
     * @throws BaException
     */
    public function modifyDomain() {
        $userDomainForm = RestHttpRequest::getRequestBody(new UserDomainForm(), true);
        $domainForm = RestClassUtils::copyFromArr(new DomainForm(), RestClassUtils::beanToArr($userDomainForm));
        $domainForm->setUsername($this->_getUsername());
        $id = RestHttpRequest::getPathValue("id");
        DomainService::modify($id, $domainForm, $this->_getUsername());
    }

    /**
     * 单个删除域名.
     * @RequestMapping(value="/{id}", method="DELETE")
     */
    public function deleteDomain() {
        $id = RestHttpRequest::getPathValue("id");
        DomainService::delete($id, $this->_getUsername());
    }

    /**
     * 设置域名状态.
     * @RequestMapping(value="/actions/state", method="POST")
     * @throws BaException
     */
    public function setDomainState() {
        $domainStateSet = RestHttpRequest::getRequestBody(new DomainStateSet(), true);
        if (empty($domainStateSet->getDomainIdList())) {
            throw new BaException(DomainConstant::DOMAIN_TO_OPERATION_CAN_NOT_NULL);
        }
        DomainService::setState($domainStateSet, $this->_getUsername());
    }

    /**
     * 批量删除域名.
     * @RequestMapping(value="/actions/delete", method="POST")
     * @throws BaException
     */
    public function deleteDomainBatch() {
        $arrId = RestHttpRequest::getRequestBody();
        if (null == $arrId || empty($arrId)) {
            throw new BaException(DomainConstant::DOMAIN_TO_DELETE_CAN_NOT_NULL);
        }
        DomainService::batchDelete($arrId, $this->_getUsername());
    }
}
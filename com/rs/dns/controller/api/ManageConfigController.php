<?php
namespace com\rs\dns\controller\api;

use com\rs\dns\controller\BaseController;
use com\rs\dns\service\ConfigService;
use restphp\http\RestHttpRequest;

/**
 * Class ManageConfigController
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/manage/configs")
 */
final class ManageConfigController extends BaseController {
    /**
     * 获取所有配置.
     * @RequestMapping(value="", method="GET")
     * @throws \restphp\exception\RestException
     */
    public function getAllConfig() {
        $infoList = ConfigService::getAll();
        $this->_success($infoList);
    }

    /**
     * @RequestMapping(value="/actions/setting", method="POST")
     * @throws \restphp\exception\RestException
     */
    public function setting() {
        $arrConfig = RestHttpRequest::getRequestBody();
        ConfigService::batchSet($arrConfig);
    }
}
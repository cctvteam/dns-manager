<?php
namespace  com\rs\dns\controller\api;

use com\rs\dns\controller\BaseController;
use com\rs\dns\service\TokenService;

/**
 * Class ApiController
 * @package com\rs\dns\controller\api
 * @RequestMapping("/api/iframe/token")
 */
final class ApiController extends BaseController {
    /**
     * @RequestMapping(value="", method="POST")
     */
    public function create() {
        $token = TokenService::create($this->_getUserId());
        $this->_success($token);
    }
}
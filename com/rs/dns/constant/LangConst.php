<?php
namespace com\rs\dns\constant;

final class LangConst {
    const USER_NOT_LOGIN = '[USER_NOT_LOGIN]';
    const USER_ACCESS_FORBIDDEN = '[USER_ACCESS_FORBIDDEN]';
}
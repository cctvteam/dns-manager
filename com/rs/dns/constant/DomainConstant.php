<?php
namespace com\rs\dns\constant;

/**
 * Class DomainConstant
 * @package com\rs\dns\constant
 */
final class DomainConstant {
    const DOMAIN_TO_OPERATION_CAN_NOT_NULL = 'DOMAIN_TO_OPERATION_CAN_NOT_NULL';
    const DOMAIN_TO_DELETE_CAN_NOT_NULL = 'DOMAIN_TO_DELETE_CAN_NOT_NULL';
    const DOMAIN_EXISTS = 'DOMAIN_EXISTS';
    const DOMAIN_NOT_EXISTS = 'DOMAIN_NOT_EXISTS';
    const DOMAIN_RECORD_NOT_EXISTS = 'DOMAIN_RECORD_NOT_EXISTS';
    const DOMAIN_PARENT_EXISTS = 'DOMAIN_PARENT_EXISTS';
    const DOMAIN_OVER_MAX_ALLOWED = 'DOMAIN_OVER_MAX_ALLOWED';
}
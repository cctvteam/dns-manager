<?php
namespace com\rs\dns\constant;

/**
 * Class ConfigConstant
 * @package com\rs\dns\constant
 */
final class ConfigConstant {
    const CODE_SERVER_KEY = 'serverKey';
    const CODE_ALLOW_HTTP_API = 'allowHttpApi';
    const CODE_ALLOW_IFRAME_API = 'allowIFrameApi';
    const CODE_ALLOW_FOREIGN_API = 'allowForeignApi';
    const CODE_FOREIGN_URL = 'foreignUrl';
    const CODE_URL_SERVER_IP = 'urlServerIp';
}
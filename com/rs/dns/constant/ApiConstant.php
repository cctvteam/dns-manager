<?php
/**
 * Created by zj.
 * User: zj
 * Date: 2020/5/24 0024
 * Time: 上午 12:07
 */

namespace com\rs\dns\constant;


class ApiConstant {
    const UNAUTHORIZED = "UNAUTHORIZED";
    const FORBIDDEN = "FORBIDDEN";
    const APPLY_ACCEPTED = "APPLY_ACCEPTED";
    const APPLY_CHECKING = "APPLY_CHECKING";

    const HTTP_API_CLOSED = 'HTTP_API_CLOSED';
    const HTTP_API_ACCOUNT_ERROR = 'HTTP_API_ACCOUNT_ERROR';
    const IFRAME_API_CLOSED = 'IFRAME_API_CLOSED';
    const IFRAME_API_TOKEN_ERROR = 'IFRAME_API_TOKEN_ERROR';
    const IFRAME_API_TOKEN_EXPIRED = 'IFRAME_API_TOKEN_EXPIRED';
}
<?php
namespace com\rs\dns\repository;

use com\rs\dns\repository\bean\UserListBean;
use restphp\driver\RestMSRepository;
use restphp\utils\RestClassUtils;

/**
 * Class UserListRepository
 * @package php\repository
 */
class  UserListRepository extends RestMSRepository {
    function __construct() {
        parent::__construct("userlist");
    }

    /**
     * @var UserListBean.
     */
    private $_userBean;

    /**
     * 通过账号查找用户.
     * @param $strAccount
     * @return mixed|null|UserListBean.
     */
    public function findOneByUserAccount($strAccount) {
        $arrDataList = $this->select(array(
            'rule' => array(
                'username' => $strAccount
            ),
            'limit' => ' top 1 ',
            'order' => ' order by id '
        ));
        $this->_userBean = null;
        if (isset($arrDataList[0])) {
            $this->_userBean = RestClassUtils::copyFromArr(new UserListBean(), $arrDataList[0]);
        }
        return $this->_userBean;
    }

    /**
     * 通用过用户ID获取用户信息.
     * @param $userId
     * @return mixed|null|UserListBean
     */
    public function findOneByUserId($userId) {
        $arrDataList = $this->select(array(
            'rule' => array(
                'id' => $userId
            ),
            'limit' => ' top 1 ',
            'order' => ' order by id '
        ));
        $this->_userBean = null;
        if (isset($arrDataList[0])) {
            $this->_userBean = RestClassUtils::copyFromArr(new UserListBean(), $arrDataList[0]);
        }
        return $this->_userBean;
    }

    /**
     * 首页数据统计.
     * @return array
     */
    public function statistics() {
        //未审核
        $uncheck = $this->countByMixRule(array(
            'verify' => 'N'
        ));

        //已拒绝
        $refuse = $this->countByMixRule(array(
            'verify' => 'ZR',
        ));

        //已禁用
        $forbidden = $this->countByMixRule(array(
            'verify' => 'ZS'
        ));

        //正常
        $normal = $this->countByMixRule(array(
            'verify' => 'Y'
        ));

        //今日注册
        $today = $this->countByMixRule(array(
            'k' => array(
                    "regtime>'" . date('Y-m-d') . " 00:00:00'"
            )
        ));

        return array(
            'uncheck' => $uncheck,
            'refuse' => $refuse,
            'normal' => $normal,
            'forbidden' => $forbidden,
            'today' => $today
        );
    }

    /**
     * 统计最近7天注册趋势.
     * @return array
     */
    public function statisticsTrend7() {
        $dataRange = array();
        for($i=6;$i>-1;$i--) {
            $dateTime = time() - ($i * 24 * 60 * 60);
            $day = array(
                'start' => date('Y-m-d 00:00:00', $dateTime),
                'end' => date('Y-m-d 23:59:59', $dateTime),
                'date' => date('Y-m-d', $dateTime)
            );
            $dataRange[] = $day;
        }

        foreach ($dataRange as &$day) {
            $countRule = array(
                'k' => array(
                    ' regtime>=? and regtime<=? ',
                    array($day['start'], $day['end'])
                )
            );
            $dayNumber = $this->countByMixRule($countRule);
            $day['num'] = $dayNumber;
        }

        return $dataRange;
    }
}
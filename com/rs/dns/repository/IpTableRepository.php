<?php
namespace com\rs\dns\repository;

use restphp\driver\RestMSRepository;

/**
 * Class IpTableRepository
 * @package com\rs\dns\repository
 */
final class IpTableRepository extends RestMSRepository {
    public function __construct()
    {
        parent::__construct('iptable');
    }
}
<?php
namespace com\rs\dns\repository;

use restphp\driver\RestMSRepository;

/**
 * Class NetRepository
 * @package com\rs\dns\repository
 */
final class NetRepository extends RestMSRepository {
    public function __construct() {
        parent::__construct('net');
    }
}
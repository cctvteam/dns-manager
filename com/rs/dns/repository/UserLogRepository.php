<?php
namespace com\rs\dns\repository;

use restphp\driver\RestMSRepository;

/**
 * Class UserLogRepository
 * @package php\repository
 */
class UserLogRepository extends RestMSRepository {
    function __construct() {
        parent::__construct('user_log');
    }
}
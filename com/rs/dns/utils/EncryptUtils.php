<?php
namespace com\rs\dns\utils;

/**
 * Class EncryptUtils
 * @package php\utils
 */
class EncryptUtils {
    /**
     * 密码加密统一入口.
     * @param $strSourcePassword
     * @return string
     */
    public static function encodePassword($strSourcePassword) {
        return strtolower(md5($strSourcePassword));
    }
}
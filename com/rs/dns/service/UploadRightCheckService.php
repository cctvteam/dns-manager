<?php
namespace com\rs\dns\service;

use com\rs\dns\constant\ApiConstant;
use com\rs\dns\exception\BaException;
use com\rs\dns\service\user\impl\UserSessionServiceImpl;
use restphp\http\RestHttpStatus;

/**
 * Class UploadRightCheckService
 * @package php\service\upload
 */
class UploadRightCheckService {
    /**
     * @param $strRightCode
     * @return mixed
     */
    public static function check($strRightCode) {
        return DIRECTORY_SEPARATOR . $strRightCode;
    }

    /**
     * 是否有管理员权限
     * @param $strRightCode
     * @return string
     * @throws BaException
     */
    public static function checkAdmin($strRightCode) {
        $userSession = new UserSessionServiceImpl();
        if ($userSession->isAdmin() || $userSession->isSuper()) {
            return DIRECTORY_SEPARATOR . $strRightCode;
        }

        throw new BaException(ApiConstant::FORBIDDEN, RestHttpStatus::Forbidden);
    }

    public static function set($param) {
        return $param;
    }

    /**
     * 访问授权验证.
     * @param $param
     * @throws BaException
     */
    public static function visit($param) {
        //web登录用户
        $userSession = new UserSessionServiceImpl();
        if ($userSession->isLogin()) {
            return;
        }

        throw new BaException(ApiConstant::FORBIDDEN, RestHttpStatus::Forbidden);
    }

    /**
     * 所有人都可以访问.
     * @param $param
     */
    public static function visitFree($param) {
        return;
    }
}
<?php
namespace com\rs\dns\service;

use com\rs\dns\controller\api\vo\CommonQuery;
use com\rs\dns\controller\api\vo\HostForm;
use com\rs\dns\controller\api\vo\HostInfo;
use com\rs\dns\controller\api\vo\Ipv6BatchForm;
use com\rs\dns\controller\api\vo\Ipv6Form;
use com\rs\dns\repository\HostCheckRepository;
use com\rs\dns\repository\IpTableV6Repository;
use restphp\biz\PageParam;
use restphp\utils\RestStringUtils;

/**
 * Class HostService
 * @package com\rs\dns\service
 */
final class HostService {
    /**
     * 获取IP分配列表.
     * @param CommonQuery $commonQuery
     * @param PageParam $pageParam
     * @return \restphp\biz\RestPageReturn
     */
    public static function getHostList($commonQuery, $pageParam) {
        $arrRule = array(
        );
        if (!RestStringUtils::isBlank($commonQuery->getKeyword())) {
            $keyword = '%' . $commonQuery->getKeyword() . '%';
            $arrRule['k1'] = array(
                ' ip like ? ',
                $keyword
            );
        }

        $arrParams = array(
            'output' => ' * ',
            'rule' => $arrRule,
            'page_param' => $pageParam,
            'order' => ' order by ip '
        );
        $hostRepository = new HostCheckRepository();
        return $hostRepository->getPageReturn($arrParams, new HostInfo());
    }

    /**
     * 添加监控地址.
     * @param HostForm $hostForm
     * @throws \ReflectionException
     */
    public static function add($hostForm) {
        // todo: 检查重复地址.
        $hostCheckRepository = new HostCheckRepository();
        $hostCheckRepository->save($hostForm);
    }

    /**
     * 修改监控分配地址.
     * @param $ip
     * @param HostForm $hostForm
     * @throws \ReflectionException
     */
    public static function modify($ip, $hostForm) {
        // todo: 检查重复地址.
        $arrRule = array(
            'ip' => $ip
        );
        $arrData = array(
            'port' => $hostForm->getPort()
        );
        $hostCheckRepository = new HostCheckRepository();
        $hostCheckRepository->update($arrData, $arrRule);
    }

    /**
     * 删除监控配置地址.
     * @param $ip
     */
    public static function delete($ip) {
        $arrRule = array(
            'ip' => $ip
        );
        $hostCheckRepository = new HostCheckRepository();
        $hostCheckRepository->delete($arrRule);
    }

    /**
     * 批量删除.
     * @param $arrId
     */
    public static function deleteBatch($arrId) {
        $strDelRule = '';
        $arrSqlParam = array();
        foreach ($arrId as $id) {
            $strDelRule .= ('' == $strDelRule ? '' : ',') . '?';
            $arrSqlParam[] = $id;
        }
        $arrRule = array(
            "k" => array(
                " ip in ({$strDelRule}) ",
                $arrSqlParam
            )
        );
        $hostCheckRepository = new HostCheckRepository();
        $hostCheckRepository->delete($arrRule);
    }

    /**
     * 批量添加
     * @param Ipv6BatchForm $hostBatchForm
     * @throws \ReflectionException
     */
    public static function batchAdd($hostBatchForm) {
        // todo: 判断重复.
        $hostCheckRepository = new HostCheckRepository();
        $arrLine = explode("\n", $hostBatchForm->getIpTxt());
        foreach ($arrLine as $line) {
            //先删除相同IP.
            $arrCol = explode(':', $line);
            $arrDeleteRule = array(
                'ip' => $arrCol[0],
            );
            $hostCheckRepository->delete($arrDeleteRule);
            //新增数.
            $hostForm = new HostForm();
            $hostForm->setIp($arrCol[0]);
            $hostForm->setPort($arrCol[1]);
            $hostCheckRepository->save($hostForm);
        }
    }
}
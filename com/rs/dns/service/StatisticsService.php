<?php
namespace com\rs\dns\service;

use com\rs\dns\repository\CrmMessageRepository;
use com\rs\dns\repository\CrmWorkRepository;
use com\rs\dns\repository\UserListRepository;

/**
 * Class StatisticsService
 * @package com\service
 */
class StatisticsService {
    /**
     * 管理员首页-会员信息统计.
     * @return array
     */
    public static function member() {
        $userRepository = new UserListRepository();
        return $userRepository->statistics();
    }

    /**
     * 管理员首页-会员注册趋势统计.
     * @return array
     */
    public static function memberTrend() {
        $userRepository = new UserListRepository();
        return $userRepository->statisticsTrend7();
    }

    /**
     * 用户首页.
     * @param $userId int 用户ID.
     * @return array
     */
    public static function userState($userId) {
        //工单统计
        $workRepository = new CrmWorkRepository();
        $work = $workRepository->stateUsers($userId);
        return array(
            'work' => $work
        );
    }
}
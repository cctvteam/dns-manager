<?php
namespace com\rs\dns\service;

use com\rs\dns\constant\ConfigConstant;
use restphp\http\RestHttpMethod;
use restphp\utils\RestClientCurlUtils;
use restphp\utils\RestLog;
use restphp\utils\RestStringUtils;

final class ForeignService {
    /**
     * 接口是否开启.
     * @return bool
     * @throws \restphp\exception\RestException
     */
    private static function isOpen() {
        return "Y" == ConfigService::getConfigValue(ConfigConstant::CODE_ALLOW_FOREIGN_API) &&
            !RestStringUtils::isBlank(ConfigService::getConfigValue(ConfigConstant::CODE_FOREIGN_URL));
    }

    /**
     * 组装完整的接口请求地址.
     * @param $uri string 接口路径.
     * @return string
     * @throws \restphp\exception\RestException
     */
    private static function getFullUrl($uri) {
        return ConfigService::getConfigValue(ConfigConstant::CODE_FOREIGN_URL) . $uri;
    }

    /**
     * 生成部
     * @param $method
     * @param $url
     * @return array
     * @throws \restphp\exception\RestException
     */
    private static function buildHeader($method, $url) {
        $serverKey = ConfigService::getConfigValue(ConfigConstant::CODE_SERVER_KEY);
        $nonce = time() . "_" . RestStringUtils::randomCode(6);
        $source = "{$method}\n{$url}\n{$nonce}\{$serverKey}";
        $sign = md5($source);
        return array(
            'authorize' => "nonce={$nonce};sign={$sign}",
            "Content-Type" => "application/json; charset=UTF-8"
        );
    }

    /**
     * GET方式通知.
     * @param $uri string 接口路径.
     * @throws \restphp\exception\RestException
     */
    public static function get($uri) {
        if (!self::isOpen()) {
            return;
        }

        try {
            //@RestClientUtils::get($uri, self::buildHeader(RestHttpMethod::GET, $uri));
            @RestClientCurlUtils::get(self::getFullUrl($uri), self::buildHeader(RestHttpMethod::GET, $uri));
        } catch (\Exception $e) {
            //忽略错误执行
            RestLog::writeLog($e->getMessage(), "foreign.err.");
        }
    }

    /**
     * POST方式通知.
     * @param $uri string 接口路径.
     * @param array $data body数据.
     * @throws \restphp\exception\RestException
     */
    public static function post($uri, $data = array()) {
        if (!self::isOpen()) {
            return;
        }

        try {
            //@RestClientUtils::post(self::getFullUrl($uri), $data, self::buildHeader(RestHttpMethod::POST, $uri));
            @RestClientCurlUtils::post(self::getFullUrl($uri), $data, self::buildHeader(RestHttpMethod::POST, $uri));
        } catch (\Exception $e) {
            //忽略错误执行
            RestLog::writeLog($e->getMessage(), "foreign.err.");
        }

    }

    /**
     * PUT方式通知.
     * @param $uri string 接口路径.
     * @param array $data body数据.
     * @throws \restphp\exception\RestException
     */
    public static function put($uri, $data = array()) {
        if (!self::isOpen()) {
            return;
        }

        try {
            //@RestClientUtils::put(self::getFullUrl($uri), $data, self::buildHeader(RestHttpMethod::PUT, $uri));
            @RestClientCurlUtils::put(self::getFullUrl($uri), $data, self::buildHeader(RestHttpMethod::PUT, $uri));
        } catch (\Exception $e) {
            //忽略错误执行
            RestLog::writeLog($e->getMessage(), "foreign.err.");
        }
    }

    /**
     * DELETE方式通知.
     * @param $uri string 接口路径.
     * @throws \restphp\exception\RestException
     */
    public static function delete($uri) {
        if (!self::isOpen()) {
            return;
        }

        try {
            //@RestClientUtils::delete($uri, self::buildHeader(RestHttpMethod::DELETE, $uri));
            @RestClientCurlUtils::delete(self::getFullUrl($uri), self::buildHeader(RestHttpMethod::DELETE, $uri));
        } catch (\Exception $e) {
            //忽略错误执行
            RestLog::writeLog($e->getMessage(), "foreign.err.");
        }
    }
}
<?php
namespace com\rs\dns\service;

use restphp\utils\RestStringUtils;

/**
 * Class MemberRandCodeService
 * @package com\service
 */
class MemberRandCodeService {
    private static $_memSessionKey = "mem_rand_code";

    /**
     * 创建随机验证码.
     * @return string
     */
    public static function createCode() {
        $strCode = RestStringUtils::randomCode(4, '21');
        $_SESSION[self::$_memSessionKey] = $strCode;
        return $strCode;
    }

    /**
     * 校验验证码.
     * @param $strCheckCode
     * @param bool $clear
     * @return bool
     */
    public static function checkCode($strCheckCode, $clear = true) {
        $strSource = isset($_SESSION[self::$_memSessionKey]) ? $_SESSION[self::$_memSessionKey] : '';
        if (RestStringUtils::isBlank($strSource)) {
            return false;
        }
        $result = strtoupper($strSource) == strtoupper($strCheckCode);
        if ($clear) {
            unset($_SESSION[self::$_memSessionKey]);
        }
        return $result;
    }
}
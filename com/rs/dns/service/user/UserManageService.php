<?php
namespace com\rs\dns\service\user;
use com\rs\dns\controller\api\vo\UserQuery;
use com\rs\dns\controller\api\vo\UserStateSet;
use restphp\biz\PageParam;
use restphp\biz\RestPageReturn;

/**
 * Interface UserManageService
 * @package php\service\user
 */
interface UserManageService {
    /**
     * 获取用户列表.
     * @param $userQuery UserQuery
     * @param $pageParam PageParam
     * @return RestPageReturn
     */
    public function getUserList($userQuery, $pageParam);

    /**
     * 添加用户.
     * @param $arrNewUser
     * @return mixed
     */
    public function addUser($arrNewUser);

    /**
     * 修改用户.
     * @param $strUserId
     * @param $arrUpdate
     * @return mixed
     */
    public function modifyUser($strUserId, $arrUpdate);

    /**
     * 删除用户.
     * @param $strUserId
     * @return mixed
     */
    public function deleteUser($strUserId);

    /**
     * 批量删除用户.
     * @param array $arrId
     * @return mixed
     */
    public function deleteUserBatch($arrId);

    /**
     * 批量设置用户状态.
     * @param UserStateSet $userStateSet
     * @return mixed
     */
    public function batchSetState($userStateSet);
}
<?php
//开发配置定义
define('APP_NAME', 'DnsManager');
session_start();

//当前环镜
define('PROJ_ENV', 'dev');

//环镜参数
$_EVN_PARAM_ALL = include('env.config.php');
$_EVN_PARAM = $_EVN_PARAM_ALL[PROJ_ENV];

//动态安装参数补充
if (file_exists(ROOT_DIR . 'runtime/install/install.config.php')) {
    $_EVN_PARAM_ADD = include (ROOT_DIR . 'runtime/install/install.config.php');
    $_EVN_PARAM = array_merge($_EVN_PARAM, $_EVN_PARAM_ADD);
}

$_DB_SQL_SERVER = isset($_EVN_PARAM) ? $_EVN_PARAM['DATABASE_SQL_SERVER'] : array();

//加载多语言
$_LANG = include('lang.config.php');

//日志目录
define('APP_LOG_DIR', 'runtime/log');
//是否记录日志：0否，1是
define('APP_LOG_SWITCH', isset($_EVN_PARAM['RECORD_LOG']) ? $_EVN_PARAM['RECORD_LOG'] : 0);

$_FILTER = array(
    //HTTP API
    array(
        'class' => '\com\rs\dns\filter\ApiFilter',
        'function' => 'run'
    ),

    //IFrame API
    array(
        'class' => '\com\rs\dns\filter\IFrameApiFilter',
        'function' => 'run'
    ),

    // 安装检查
    array(
        'class' => '\com\rs\dns\service\InstallService',
        'function' => 'run'
    ),

    // 模板资源调度
    array(
        'class' => '\com\rs\dns\filter\TplFilter',
        'function' => 'run'
    )
);
/**
 * 用户中心-域名解析-DNS-负载均衡.
 * @type {Vue}
 */
let vmMemberRecordsBalance = new Vue({
    el: '#member-records-balance',
    data () {
        return {
            domain: '',
            recordId: '',
            currentBalance: '0',

            //列表所需数据
            listLoading: false,
            tableData : [],

            //修改
            form: {
                balance: '0'
            },
            rules: {
                balance: [
                    { required: true, message: '请选择需要变更的模式', trigger: 'blur' }
                ]
            }

        }
    },
    methods: {
        loadPage () {
            let domain = QiServletUtils.get("domain");
            let recordId = QiServletUtils.get("recordId");
            if (QiStringUtils.isBlank(domain) || QiStringUtils.isBlank(recordId)) {
                this.$message.error('参数不正确');
                return;
            }
            this.domain = domain;
            this.recordId = recordId;

            this.loadData();
        },
        /**
         * 架载统计数据.
         */
        loadData () {
            this.listLoading = true;
            let url = '/api/user/records/' + this.recordId + '/balances';
            url += (iFrameToken ? ('?iframe-token=' + iFrameToken) : '');
            QiRestClient.get(url, (data) => {
                this.tableData = data;
                this.currentBalance = data[0].balance;
                this.form.balance = data[0].balance;
                this.listLoading = false;
            }, (data) => {
                this.$message.error(data.message);
                this.listLoading = false;
            });
        },
        /**
         * 保存数据操作.
         */
        saveEdit () {
            let auxList = [];
            let err = false;
            this.tableData.forEach((row, index) => {
                if (!QiStringUtils.isNumber(row.aux) || row.aux < 1) {
                    this.$message.error('第'+(index+1)+'行的优先级值不正确');
                    err = true;
                    return;
                }
                auxList.push({
                    id: row.id,
                    aux: row.aux
                })
            });
            if (err) {
                return;
            }
            let success =  (data) => {
                this.$message.success('操作成功');
                this.dialogFormVisible = false;
                this.loadData();
            };
            let error = (data) => {
                this.$message.error(data.message);
            };

            let data = {
                balance: this.form.balance,
                auxList: auxList
            }

            QiRestClient.post('/api/user/records/' + this.recordId + '/balances' + (iFrameToken ? ('?iframe-token=' + iFrameToken) : ''), data, success, error);
        }
    },
    mounted: function () {
        this.loadPage();
    }
});
let vmInstall = new Vue({
    el: '#main-block',
    data: function() {
        let validateUsername =  (rule, value, callback) => {
            if (QiStringUtils.isEnNumber(value)) {
                callback();
            } else {
                callback(new Error(rule.message));
            }
        };

        return {
            active: 0,
            loadingTitle: '环镜检查中',
            checkLoading: true,

            firstSuccess: 0,
            checkRow: [],

            twoSuccess: 1,
            ruleForm: {
                dbServer: '',
                dbPort: '1433',
                dbUsername: '',
                dbPassword: '',
                dbName: 'WinMyDNS',
                manageUsername: '',
                managePassword: ''
            },
            rules: {
                dbServer: [
                    { required: true, message: '请输入数据库地址', trigger: 'blur' }
                ],
                dbPort: [
                    { required: true, message: '请输入数据库端口', trigger: 'blur' }
                ],
                dbUsername: [
                    { required: true, message: '请输入数据库用户', trigger: 'blur' }
                ],
                dbPassword: [
                    { required: true, message: '请输入数据库用户链接密码', trigger: 'blur' }
                ],
                dbName: [
                    { required: true, message: '请输入数据库名称', trigger: 'blur' }
                ],
                manageUsername: [
                    { required: true, min:4, max:16, message: '请输入网站管理平台管理账号，长度为4-16字符', trigger: 'blur' },
                    { validator: validateUsername, message: '管理账号必须为英文数字组'}
                ],
                managePassword: [
                    { required: true, min:6, max:16, message: '请输入网站管理平台管理账号登录密码, 长度为6-16字符', trigger: 'blur' }
                ]
            },

            thirdSuccess: 1
        }
    },
    methods: {
        /**
         * 是否可以进入下一步
         */
        canNext: function () {
            if (this.active == 0) {
                return this.firstSuccess > 0;
            } else if (this.active == 1) {
                return this.twoSuccess > 0;
            }
            return false;
        },

        /**
         * 是否可以上一步.
         */
        canBack: function () {
            if (this.active == 0) {
                return false;
            } else if (this.active == 1) {
                return true;
            } else {
                return this.thirdSuccess < 1
            }
        },

        /**
         * 进入下一步.
         */
        goNext: function () {
            if (this.active == 1) {
                var flag = false;
                this.$refs['ruleForm'].validate(function (valid) {
                    flag = valid;
                });
                if (!flag) {
                    return false;
                }
            }
            if (this.canNext()) {
                this.active++;
            }

            if (this.active == 2) {
                this.makeInstall();
            }
        },

        /**
         * 返回上一步
         */
        goPre: function () {
            if (this.active > 0) {
                this.active--;
            }

            if (this.active == 0) {
                this.checkEvn();
            }
        },

        /**
         * 检查安装环镜.
         */
        checkEvn: function () {
            var self = this;
            self.checkLoading = true;
            self.loadingTitle = '环镜检查中';
            QiRestClient.get('/install/env', function (data) {
                self.checkLoading = false;
                if (parseInt(data.result) > 0) {
                    self.checkRow = data.row;
                    if (parseInt(data.result) > 1) {
                        self.firstSuccess = 1;
                    }
                } else {
                    $('#checkResult').html(data.msg);
                }
            }, function (data) {
                self.checkLoading = false;
                $('#checkResult').html("获取环镜检查结果失败");
            });
        },

        /**
         * 安装.
         */
        makeInstall: function () {
            var self = this;
            self.checkLoading = true;
            self.loadingTitle = '安装中';
            self.thirdSuccess = 1;
            QiRestClient.post('/install/make', self.ruleForm, function (data) {
                self.checkLoading = false;
                $('#installResult').html(data.msg);
                if (parseInt(data.result) != 1) {
                    self.thirdSuccess = 0;
                } else {
                    var html = '。<br /><a href="/" target="_blank">进入管理首页</a><br /><br />';
                    $('#installResult').append(html);
                }
            }, function (data) {
                self.checkLoading = false;
                self.thirdSuccess = 0;
                $('#installResult').html("安装失败:" + data.message);
            });
        }
    },
    mounted: function () {
        this.checkEvn();
    }
});
/**
 * 管理后台.
 * @type {Vue}
 */
let vmMember = new Vue({
    el: '#main-block',
    data: function () {
        return {
            activeIndex: '1',
            menuMap: {
                '1': '/member-config',
                '2-1': '/member-domains',
                '2-2': '/member-records',
                '2-2-1': '/member-records-balance',
                '5': '/member-info'
            }
        }
    },
    methods: {
        /**
         * 加载页面
         * @param menu
         * @param fun
         */
        goToPage: function (menu, fun) {
            $('#mainDiv').tpl(this.menuMap[menu] + (iFrameToken ? ('?iframe-token=' + iFrameToken) : ''), {}, function () {
                if (fun) {
                    fun();
                }
            });
        },
        /**
         * 采单选择.
         * @param index
         */
        selectMenu: function (index) {
            if (index != '10') {
                location.href = '?menu=' + index + (iFrameToken ? ('&iframe-token=' + iFrameToken) : '');
            }
            if (index == '10') {
                this.$confirm('确定需要退出登录吗？', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(function (){
                    QiRestClient.delete('/api/user/session' + (iFrameToken ? ('?iframe-token=' + iFrameToken) : ''), () => {
                        location.reload();
                    });
                }).catch(function(){
                });
            }
        },
        /**
         * 加载主内容.
         */
        loadBody: function () {
            var index = QiServletUtils.get("menu");
            index = index || '1';
            this.goToPage(index);
            this.activeIndex = index;
        }
    },
    mounted: function () {
        this.loadBody();
    }
});
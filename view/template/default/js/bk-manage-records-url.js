/**
 * 管理后台中心首页.
 * @type {Vue}
 */
let vmManageRecordsUrl = new Vue({
    el: '#manage-records-url',
    data: function () {
        return {
            typeList: [
                {value:'0', label: '显示转发'},
                {value:'1', label: '隐性转发'}
            ],
            netList: [],
            //列表所需数据
            formQuery: {
                domain: '',
                username: ''
            },
            listLoading: false,
            tableData : [],
            page: {
                current: 1,
                total: 0,
                page:1,
                pageSize:20
            },
            selectRows: [],

            //新增|修改
            isNew: false,
            dialogFormVisible: false,
            form: {
                origin: '',
                name: '',
                URLtype: '0',
                netid: '0',
                URL: '',
                title: ''
            },
            tmpForm: {
                origin: '',
                name: '',
                URLtype: '0',
                netid: '0',
                URL: '',
                title: ''
            },
            rules: {
                origin: [
                    { required: true, message: '请输入域名', trigger: 'blur' }
                ],
                URLtype: [
                    { required: true, message: '请选择转发类型', trigger: 'blur' }
                ],
                netid: [
                    { required: true, message: '请选择网络组', trigger: 'blur' }
                ],
                URL: [
                    { required: true, message: '请输入转发地址', trigger: 'blur' }
                ]
            },

            //旧数据处理
            oldBtnLoading: false
        }
    },
    methods: {
        loadPage () {
            let domain = QiServletUtils.get("domain");
            if (!QiStringUtils.isBlank(domain)) {
                this.formQuery.domain = decodeURI(domain);
                this.tmpForm.origin = decodeURI(domain);
            }

            let url = '/api/manage/nets/action/collect' + (iFrameToken ? ('?iframe-token=' + iFrameToken) : '');
            QiRestClient.get(url, (data) => {
                let def = {id: '0', netname: '默认'};
                data.unshift(def)
                this.netList = data;
            }, (data) => {
                this.$message.error(data.message);
            });

            this.loadData();
        },
        /**
         * 架载统计数据.
         */
        loadData () {
            this.listLoading = true;
            let url = '/api/manage/urls?';
            let param = $.extend({}, this.formQuery, {page: this.page.page, size: this.page.pageSize});
            url += QiServletUtils.mapToQueryString(param);
            url += (iFrameToken ? ('&iframe-token=' + iFrameToken) : '');
            QiRestClient.get(url, (data) => {
                this.tableData = data.items;
                this.page.total = data.total;
                this.listLoading = false;
            }, (data) => {
                this.$message.error(data.message);
                this.listLoading = false;
            });
        },
        /**
         * 查询触发.
         */
        onQuery () {
            this.page.current = 1;
            this.page.page = 1;
            this.loadData();
        },
        /**
         * 分页触发事件.
         * @param val
         */
        handleCurrentChange (val) {
            this.page.page = val;
            console.log("当前页数："+val);
            this.loadData();
        },
        /**
         * 选择事件.
         */
        handleSelectionChange (val) {
            this.selectRows = val;
        },
        /**
         * 新增.
         */
        showAdd () {
            this.isNew = true;
            this.form = Object.assign({}, this.tmpForm);
            this.dialogFormVisible = true;
        },
        /**
         * 标注.
         * @param row
         */
        showEdit (row) {
            this.isNew = false;
            this.form = Object.assign({}, row);
            this.form.origin = this.form.origin.substr(0, this.form.origin.length - 1);
            this.dialogFormVisible = true;
        },
        /**
         * 保存数据操作.
         */
        saveEdit () {
            this.$refs['form'].validate((valid) => {
                if (valid) {
                    let success =  (data) => {
                        this.$message.success('操作成功');
                        this.dialogFormVisible = false;
                        this.loadData();
                    };
                    let error = (data) => {
                        this.$message.error(data.message);
                    };

                    if (this.isNew) {
                        QiRestClient.post('/api/manage/urls' + (iFrameToken ? ('?iframe-token=' + iFrameToken) : ''), this.form, success, error);
                    } else {
                        QiRestClient.put('/api/manage/urls/' + this.form.ID + (iFrameToken ? ('?iframe-token=' + iFrameToken) : ''), this.form, success, error);
                    }
                }
            });
        },
        /**
         * 删除选中域名.
         */
        deleteAll () {
            if (this.selectRows.length < 1) {
                this.$message.error('请选择需要删除的转发.');
                return;
            }
            this.$confirm('确定需要移除选中转发吗？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then( () => {
                let domains = [];
                this.selectRows.forEach((row) => {
                    domains.push(row.ID);
                });
                QiRestClient.post('/api/manage/urls/actions/delete' + (iFrameToken ? ('?iframe-token=' + iFrameToken) : ''),  domains, () => {
                    this.$message.success('操作成功');
                    this.loadData();
                },  (data) => {
                    this.$message.error(data.message);
                })
            });
        },
        /**
         * 旧数据关联域名记录.
         */
        dealOld () {
            this.$confirm('进行此操作前，请先对原数据进行备份。确定需要现在进行此操作吗？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then( () => {
                this.oldBtnLoading = true;
                QiRestClient.post('/api/manage/urls/actions/old' + (iFrameToken ? ('?iframe-token=' + iFrameToken) : ''), {}, () => {
                    this.$message.success('操作成功');
                    this.loadData();
                    this.oldBtnLoading = false;
                },  (data) => {
                    this.$message.error(data.message);
                    this.oldBtnLoading = false;
                })
            });
        },
        /**
         * 删除.
         * @param row
         */
        deleteMessage (row) {
            let self = this;
            this.$confirm('确定需要移除选中记录吗？', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then( () => {
                QiRestClient.delete('/api/manage/urls/' + row.ID + (iFrameToken ? ('?iframe-token=' + iFrameToken) : ''),  () => {
                    self.$message.success('操作成功');
                    self.loadData();
                },  (data) => {
                    self.$message.error(data.message);
                })
            });
        }
    },
    mounted: function () {
        this.loadPage();
    }
});
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>软盛DNS智能解析系统-初始安装</title>
    <link rel="icon" href="images/favicon.ico" />
    <script src="js/lib/jquery/jquery.min.js"></script>
    <script src="js/lib/77/StringUtils.js"></script>
    <script src="js/lib/77/ServletUtils.js"></script>
    <script src="js/lib/77/RestClientUtils.js"></script>
    <script src="js/lib/77/tpl.js"></script>
    <script src="js/lib/77/DateUtils.js"></script>

    <link rel="stylesheet" href="js/lib/element-ui/lib/theme-chalk/index.css">
    <!-- import Vue before Element -->
    <script src="js/lib/vue/dist/vue.js"></script>
    <!-- import JavaScript -->
    <script src="js/lib/element-ui/lib/index.js"></script>
</head>
<body>
<div id="main-block">
    <table align="center" width="800" style="border: 0px">
        <tr>
            <td>
                <el-steps :active="active" finish-status="success">
                    <el-step title="检查环镜"></el-step>
                    <el-step title="配置代理信息"></el-step>
                    <el-step title="安装"></el-step>
                </el-steps>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width:100%; border: 2px solid #e6e6e6; min-height: 300px; padding: 20px"
                     v-loading="checkLoading"
                     :element-loading-text="loadingTitle"
                     element-loading-spinner="el-icon-loading"
                     element-loading-background="rgba(0, 0, 0, 0.8)">

                    <!-- 第一步 -->
                    <div v-if="active==0" id="checkResult">
                        <div v-for="item in checkRow" style="">
                            检查<span v-html="item.name"></span>(<span v-html="item.file"></span>)是否有操作权限：<i class="el-icon-check" v-if="item.flag==1" style="color: #0aaf52"></i><i class="el-icon-close" v-if="item.flag==0" style="color: #FF0000"></i>
                        </div>
                    </div>

                    <!-- 第二步 -->
                    <el-form v-if="active==1" :model="ruleForm" :rules="rules" ref="ruleForm" label-width="180px" class="demo-ruleForm" size="small">
                        <el-divider content-position="left" size="small">数据库信息</el-divider>
                        <el-form-item label="数据库地址：" prop="dbServer">
                            <el-input size="small" v-model="ruleForm.dbServer" placeholder="如：192.168.1.70" style="width: 300px;"></el-input>
                        </el-form-item>
                        <el-form-item label="远程端口：" prop="dbPort">
                            <el-input-number size="small" v-model="ruleForm.dbPort" style="width: 120px;"></el-input-number>
                        </el-form-item>
                        <el-form-item label="数据库用户：" prop="dbUsername">
                            <el-input size="small" v-model="ruleForm.dbUsername" placeholder="如：winmydns" style="width: 300px;"></el-input>
                            注：该用户需要有查询表结构和创建表的权限。
                        </el-form-item>
                        <el-form-item label="数据库用户密码：" prop="dbPassword">
                            <el-input size="small" v-model="ruleForm.dbPassword" show-password placeholder="如：akls@ad2w" style="width: 300px;"></el-input>
                        </el-form-item>
                        <el-form-item label="数据库名：" prop="dbName">
                            <el-input size="small" v-model="ruleForm.dbName" placeholder="如：winmydns" style="width: 300px;"></el-input>
                        </el-form-item>
                        <el-divider content-position="left" size="small">网站管理信息</el-divider>
                        <el-form-item label="管理员账号：" prop="manageUsername">
                            <el-input size="small" v-model="ruleForm.manageUsername" placeholder="如：rskj" style="width: 300px;"></el-input>
                        </el-form-item>
                        <el-form-item label="管理登录密码：" prop="managePassword">
                            <el-input size="small" v-model="ruleForm.managePassword" show-password placeholder="如：ad34212sdW#ds" style="width: 300px;"></el-input>
                        </el-form-item>
                    </el-form>

                    <div v-if="active==2" id="installResult"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <el-button plain :disabled="!canBack()" @click="goPre">上一步</el-button>
                <el-button plain :disabled="!canNext()" @click="goNext">下一步</el-button>
            </td>
        </tr>
    </table>
</div>
</body>
<script type="text/javascript" src="js/bk-install.js"></script>
</html>
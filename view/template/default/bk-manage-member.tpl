<div id="manage-member">
    <div style="padding: 20px; margin-top: -50px">
        <div style="padding: 10px">
            <el-breadcrumb separator-class="el-icon-arrow-right">
                <el-breadcrumb-item>会员管理</el-breadcrumb-item>
            </el-breadcrumb>
        </div>
        <div style="border: 1px solid #f7f7f7; padding: 30px">
            <el-row>
                <el-form :inline="true" :model="formQuery" class="demo-form-inline">
                    <el-form-item label="账号：">
                        <el-input v-model="formQuery.account" size="small" style="width: 160px" placeholder="账号" clearable></el-input>
                    </el-form-item>
                    <el-form-item label="手机号：">
                        <el-input v-model="formQuery.mobile" size="small" style="width: 160px" placeholder="手机号" clearable></el-input>
                    </el-form-item>
                    <el-form-item label="真实姓名：">
                        <el-input v-model="formQuery.realName" size="small" style="width: 160px" placeholder="真实姓名" clearable></el-input>
                    </el-form-item>
                    <el-form-item label="状态：">
                        <el-select v-model="formQuery.verify" placeholder="请选择状态" size="small" clearable>
                            <el-option label="未审核" value="N"></el-option>
                            <el-option label="已审核" value="Y"></el-option>
                            <el-option label="已拒绝" value="ZR"></el-option>
                            <el-option label="已禁用" value="ZS"></el-option>
                        </el-select>
                    </el-form-item>
                    <el-form-item>
                        <el-button type="primary" @click="onQuery">查询</el-button>
                    </el-form-item>
                </el-form>
            </el-row>
            <el-row>
                <el-button-group>
                    <el-button type="danger" size="small" icon="el-icon-s-check" style="float: left" @click="deleteAll">批量删除</el-button>
                    <el-button type="success" size="small" icon="el-icon-delete" style="float: left" @click="showMark">批量审核</el-button>
                </el-button-group>
                <el-button type="primary" size="small" icon="el-icon-circle-plus-outline" style="float: right" @click="showAdd">新增</el-button>
            </el-row>
            <el-row style="margin-top: 10px">
                <template>
                    <el-table
                            :data="tableData"
                            style="width: 100%"
                            border
                            v-loading="listLoading"
                            stripe
                            size="small"
                            @selection-change="handleSelectionChange">
                        <el-table-column
                                type="selection"
                                width="55">
                        </el-table-column>
                        <el-table-column
                                prop="username"
                                label="账号">
                        </el-table-column>
                        <el-table-column
                                prop="realname"
                                label="姓名">
                        </el-table-column>
                        <el-table-column
                                prop="tel"
                                label="手机号">
                        </el-table-column>
                        <el-table-column
                                prop="isadmin"
                                label="权限">
                            <template scope="scope">
                                <span v-if="scope.row.isadmin == 'Y'" style="color: red">管理员</span>
                                <span v-if="scope.row.isadmin != 'Y'" style="color: green">普通用户</span>
                            </template>
                        </el-table-column>
                        <el-table-column
                                prop="soanum"
                                label="每个域名最大记录数">
                            <template scope="scope">
                                <a :href="'?menu=2-1&username=' + scope.row.username" v-html="scope.row.soanum"></a>
                            </template>
                        </el-table-column>
                        <el-table-column
                                prop="regtime"
                                label="注册时间">
                        </el-table-column>
                        <el-table-column
                                prop="verify"
                                label="状态">
                            <template scope="scope">
                                <span v-if="scope.row.verify == 'N'" style="color: red">未审核</span>
                                <span v-if="scope.row.verify == 'Y'" style="color: green">已审核</span>
                                <span v-if="scope.row.verify == 'ZR'" style="color: goldenrod">已拒绝</span>
                                <span v-if="scope.row.verify == 'ZS'" style="color: gray">已禁用</span>
                            </template>
                        </el-table-column>
                        <el-table-column
                                width="260"
                                label="操作">
                            <template scope="scope">
                                <el-button type="primary" size="small" icon="el-icon-edit" @click="edit(scope.row)" plain>修改</el-button>
                                <el-button type="danger" size="small" icon="el-icon-delete" @click="deleteMessage(scope.row)" plain>删除</el-button>
                            </template>
                        </el-table-column>
                    </el-table>
                </template>
                <div class="page-block" >
                    <el-pagination
                            @current-change="handleCurrentChange"
                            layout="total,prev, pager, next"
                            :total= page.total
                            :page-size = page.pageSize
                            class="page"
                    >
                    </el-pagination>
                </div>
            </el-row>
        </div>

        <el-dialog :title="isNew ? '新增账号' : '修改账号'" :visible.sync="dialogFormVisible">
            <el-form :model="form" ref="form" :rules="isNew ? rulesNew : rulesModify" label-width="260px" size="small">
                <el-form-item label="账号：" prop="username">
                    <el-input placeholder="请输入账号" v-model="form.username" size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="密码：" prop="password">
                    <el-input :placeholder="isNew ? '请输入密码' : '为空表示不修改'" v-model="form.password" show-password size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="权限：" prop="isadmin">
                    <el-select v-model="form.isadmin" placeholder="请选择权限" size="small">
                        <el-option label="普通用户" value="N"></el-option>
                        <el-option label="管理员" value="Y"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="姓名：" prop="realname">
                    <el-input placeholder="请输入姓名" v-model="form.realname" size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="手机：" prop="tel">
                    <el-input placeholder="请输入手机号" v-model="form.tel" size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="状态：" prop="verify">
                    <el-select v-model="form.verify" placeholder="请选择状态" size="small">
                        <el-option label="未审核" value="N"></el-option>
                        <el-option label="已审核" value="Y"></el-option>
                        <el-option label="已拒绝" value="ZR"></el-option>
                        <el-option label="已禁用" value="ZS"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="最大域名数：" prop="soanum">
                    <el-input-number placeholder="请输入最大域名数" v-model="form.soanum" :precision="0" size="small" style="width: 280px"></el-input-number>
                </el-form-item>
                <el-form-item label="每个域名最大记录数：" prop="rrnum">
                    <el-input-number placeholder="请输入最大记录数" v-model="form.rrnum" :precision="0" size="small" style="width: 280px"></el-input-number>
                </el-form-item>
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="dialogFormVisible = false" size="small">取 消</el-button>
                <el-button type="primary" @click="saveEdit" size="small">确 定</el-button>
            </div>
        </el-dialog>

        <el-dialog title="批量修改状态" :visible.sync="dialogBatchStateFormVisible">
            <el-form :model="stateForm" ref="stateForm" :rules="stateRule" label-width="260px" size="small">
                <el-form-item label="状态：" prop="verify">
                    <el-select v-model="stateForm.verify" placeholder="请选择状态" size="small">
                        <el-option label="未审核" value="N"></el-option>
                        <el-option label="已审核" value="Y"></el-option>
                        <el-option label="已拒绝" value="ZR"></el-option>
                        <el-option label="已禁用" value="ZS"></el-option>
                    </el-select>
                </el-form-item>
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="dialogBatchStateFormVisible = false" size="small">取 消</el-button>
                <el-button type="primary" @click="saveMark" size="small">确 定</el-button>
            </div>
        </el-dialog>
    </div>
</div>
<script>
    var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
</script>
<script language="JavaScript" src="js/bk-manage-member.js"></script>
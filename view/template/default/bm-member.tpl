<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>会员中心-软盛DNS智能解析系统</title>
    <link rel="icon" href="images/favicon.ico" />

    <script src="js/lib/jquery/jquery.min.js"></script>
    <script src="js/lib/77/StringUtils.js"></script>
    <script src="js/lib/77/ServletUtils.js"></script>
    <script src="js/lib/77/RestClientUtils.js"></script>
    <script src="js/lib/77/tpl.js"></script>
    <script src="js/lib/77/DateUtils.js"></script>

    <link rel="stylesheet" href="js/lib/element-ui/lib/theme-chalk/index.css">
    <!-- import Vue before Element -->
    <script src="js/lib/vue/dist/vue.js"></script>
    <!-- import JavaScript -->
    <script src="js/lib/element-ui/lib/index.js"></script>
    <link rel="stylesheet" href="css/ht.css">
    <link rel="stylesheet" href="//at.alicdn.com/t/font_2188689_u5h5i4vgj8q.css">
    <script src="js/lib/echarts/echarts.min.js"></script>
    <script>
        var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
    </script>
</head>
<body>
<div id="main-block" <?php if (\restphp\tpl\RestTpl::get("isIFrame") == "Y") { ?>style="display: none"<?php } ?>>
    <div style="width: 100%; background-color: #545c64;display: inline-block;">
        <div style="min-width: 1000px">
            <div style="width: 290px; float: left">
                <img src="images/logo.png" style="margin-top: 10px; height: 30px; margin-left: 20px; float: left">
                <div style="margin-top: 10px; line-height: 30px; padding-left: 10px; color: #ffffff; float: left">管理中心</div>
            </div>
            <div style="width: 900px; float: left">
                <el-menu
                        :default-active="activeIndex"
                        class="el-menu-demo"
                        background-color="#545c64"
                        text-color="#fff"
                        active-text-color="#ffd04b"
                        mode="horizontal"
                        @select="selectMenu"
                        style="border: none">
                    <el-menu-item index="1">管理首页</el-menu-item>
                    <el-submenu index="2">
                        <template slot="title">解析管理</template>
                        <el-menu-item index="2-1">域名列表</el-menu-item>
                        <el-menu-item index="2-2">解析记录</el-menu-item>
                    </el-submenu>
                    <el-menu-item index="5">会员信息</el-menu-item>
                    <el-menu-item index="10">退出管理</el-menu-item>
                </el-menu>
            </div>
        </div>
    </div>
</div>
<div style="margin-top: 50px" id="mainDiv">

</div>
</body>
<script language="JavaScript" src="js/bm-member.js"></script>
</html>
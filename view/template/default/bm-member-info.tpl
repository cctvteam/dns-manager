<div id="member-info">
    <el-row style="margin-left: 100px">
        <el-form :model="form" ref="form" :rules="rules" label-width="200px" size="small">
            <el-form-item label="用户账号：" prop="username">
                <span v-html="form.username"></span>
            </el-form-item>
            <el-form-item label="密码：" prop="password">
                <el-input placeholder="请输入密码" v-model="form.password" show-password size="small" style="width: 280px"></el-input> 为空表示不修改
            </el-form-item>
            <el-form-item label="真实姓名：" prop="realname">
                <el-input placeholder="请输入真实姓名" v-model="form.realname" size="small" style="width: 280px"></el-input>
            </el-form-item>
            <el-form-item label="手机号码：" prop="tel">
                <el-input placeholder="请输入手机号" v-model="form.tel" size="small" style="width: 280px"></el-input>
            </el-form-item>
            <el-form-item label="最多可添加域名数：" prop="soanum">
                <span v-html="form.soanum"></span>
            </el-form-item>
            <el-form-item label="每个域名最大解析数：" prop="rrnum">
                <span v-html="form.rrnum"></span>
            </el-form-item>
            <el-form-item label="注册时间：" prop="regtime">
                <span v-html="form.regtime"></span>
            </el-form-item>
        </el-form>
        <div style="padding-left: 20%">
            <el-button type="primary" @click="saveModify">保存修改</el-button>
        </div>
    </el-row>
</div>
<script>
    var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
</script>
<script src="js/bm-member-info.js"></script>
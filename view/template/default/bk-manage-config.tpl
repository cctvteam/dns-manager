<div id="manage-config">
    <el-row style="margin-left: 100px">
        <el-form :model="form" ref="form" :rules="rules" label-width="200px" size="small">
            <el-form-item label="管理级接口密钥：" prop="serverKey">
                <el-input placeholder="请输入接口密钥" v-model="form.serverKey" size="small" style="width: 280px"></el-input>
                <el-button type="success" size="small" @click="buildServerKey" plain>随机生成</el-button>
            </el-form-item>
            <el-form-item label="是否开启Http Api：" prop="allowHttpApi">
                <el-switch
                        v-model="form.allowHttpApi"
                        active-color="#13ce66"
                        inactive-color="#ff4949"
                        active-text="开"
                        inactive-text="关"
                        active-value="Y"
                        inactive-value="N">
                </el-switch>
            </el-form-item>
            <el-form-item label="是否开启IFrame Api：" prop="allowIFrameApi">
                <el-switch
                        v-model="form.allowIFrameApi"
                        active-color="#13ce66"
                        inactive-color="#ff4949"
                        active-text="开"
                        inactive-text="关"
                        active-value="Y"
                        inactive-value="N">
                </el-switch>
            </el-form-item>
            <el-form-item label="对外通知 Api：" prop="allowForeignApi">
                <el-switch
                        v-model="form.allowForeignApi"
                        active-color="#13ce66"
                        inactive-color="#ff4949"
                        active-text="开"
                        inactive-text="关"
                        active-value="Y"
                        inactive-value="N">
                </el-switch>
            </el-form-item>
            <el-form-item label="对外通知服务地址：" prop="foreignUrl">
                <el-input placeholder="请输入对外通知服务地址" v-model="form.foreignUrl" size="small" style="width: 280px"></el-input>
            </el-form-item>
            <el-form-item label="URL转发服务IP：" prop="urlServerIp">
                <el-input placeholder="请输入RL转发服务IP" v-model="form.urlServerIp" size="small" style="width: 280px"></el-input>
            </el-form-item>
        </el-form>
        <div style="padding-left: 20%">
            <el-button type="primary" @click="saveModify">保存修改</el-button>
        </div>
    </el-row>
</div>
<script>
    var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
</script>
<script src="js/bk-manage-config.js"></script>
<div id="manage-records-balance">
    <div style="padding: 20px;">

        <div>
            <span>域名<b v-html="domain"></b>的负载均衡设置。</span><a href="javascript:history.back()">&gt;&gt;返回上一页</a>
        </div>
        <div style="border: 1px solid #f7f7f7">
            <el-row style="margin-top: 10px">
                <template>
                    <el-table
                            :data="tableData"
                            style="width: 100%"
                            border
                            stripe
                            v-loading="listLoading"
                            size="small">
                        <el-table-column
                                prop="name"
                                label="主机名">
                        </el-table-column>
                        <el-table-column
                                prop="type"
                                label="类型">
                        </el-table-column>
                        <el-table-column
                                prop="data"
                                label="值">
                        </el-table-column>
                        <el-table-column
                                prop="aux"
                                width="160"
                                label="优先级">
                            <template scope="scope">
                                <el-input-number placeholder="请输入优先级" v-model="scope.row.aux" :min="0" size="small" :precision="0" style="width: 120px"></el-input-number>
                            </template>
                        </el-table-column>
                        <el-table-column
                                prop="netname"
                                label="解析组">
                            <template scope="scope">
                                <span v-if="!scope.row.netname || scope.row.netname==''">默认</span>
                                <span v-else v-html="scope.row.netname"></span>
                            </template>
                        </el-table-column>
                        <el-table-column
                                prop="origin"
                                label="域名">
                        </el-table-column>
                        <el-table-column
                                prop="username"
                                label="所属会员">
                        </el-table-column>
                    </el-table>
                </template>
            </el-row>
            <el-row style="font-size: 12px; line-height: 30px">
                当前负载均衡模式为：<span v-if="currentBalance=='0'" style="color: #ff0000">常规模式</span>
                <span v-if="currentBalance=='1'" style="color: #ff0000">备份模式</span>
                <span v-if="currentBalance=='2'" style="color: #ff0000">自动模式</span>
            </el-row>
            <el-row style="font-size: 12px; line-height: 30px">
                变更为：<el-select v-model="form.balance" placeholder="请选择类型" size="small">
                    <el-option label="常规模式" value="0"></el-option>
                    <el-option label="备份模式" value="1"></el-option>
                    <el-option label="自动模式" value="2"></el-option>
                </el-select>

                <el-button type="primary" @click="saveEdit" size="small">确 定</el-button>
                <br />
                负载均衡模式说明：
                <br />
                A) 常规模式：按优先级百份比分配，优先级数字大的记录被解析到的次数多。
                <br />
                B) 备份模式：只解析到优先级数字最大的记录，当优先级数字最大的IP宕机时，解析到优先级数字次大的记录。
                <br />
                C) 自动模式：解析到所有记录上，由客户端自行选择使用哪个IP。
            </el-row>
        </div>
    </div>
</div>
<script>
    var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
</script>
<script language="JavaScript" src="js/bk-manage-records-balance.js"></script>
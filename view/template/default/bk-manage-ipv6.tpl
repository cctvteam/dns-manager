<div id="manage-ipv6">
    <div style="padding: 20px; margin-top: -50px">
        <div style="padding: 10px">
            <el-breadcrumb separator-class="el-icon-arrow-right">
                <el-breadcrumb-item>IPV6分配表</el-breadcrumb-item>
            </el-breadcrumb>
        </div>
        <div style="border: 1px solid #f7f7f7; padding: 30px">
            <el-row>
                <el-form :inline="true" :model="formQuery" class="demo-form-inline">
                    <el-form-item label="关键字：">
                        <el-input v-model="formQuery.keyword" size="small" style="width: 160px" placeholder="IP地址或备注" clearable></el-input>
                    </el-form-item>
                    <el-form-item label="网络组：">
                        <el-select v-model="formQuery.netId" placeholder="请选择网络组" size="small" clearable>
                            <el-option v-for="item in netList" :label="item.netname" :value="item.id"></el-option>
                        </el-select>
                    </el-form-item>
                    <el-form-item>
                        <el-button type="primary" size="small" @click="onQuery">查询</el-button>
                    </el-form-item>
                </el-form>
            </el-row>
            <el-row>
                <el-button-group>
                    <el-button type="danger" size="small" icon="el-icon-s-check" style="float: left" @click="deleteAll">批量删除</el-button>
                </el-button-group>
                <el-button-group style="float: right">
                    <el-button type="primary" icon="el-icon-circle-plus-outline" size="small" @click="showAdd">新增</el-button>
                    <el-button type="primary" icon="el-icon-document-add" size="small" @click="showAddBatch">批量新增</el-button>
                </el-button-group>
            </el-row>
            <el-row style="margin-top: 10px">
                <template>
                    <el-table
                            :data="tableData"
                            style="width: 100%"
                            border
                            stripe
                            v-loading="listLoading"
                            size="small"
                            @selection-change="handleSelectionChange">
                        <el-table-column
                                type="selection"
                                width="55">
                        </el-table-column>
                        <el-table-column
                                prop="netname"
                                label="网络">
                            <template scope="scope">
                                <span v-html="getNetName(scope.row.netid)"></span>
                            </template>
                        </el-table-column>
                        <el-table-column
                                prop="ip"
                                label="IP">
                        </el-table-column>
                        <el-table-column
                                prop="mask"
                                label="子网掩码">
                        </el-table-column>
                        <el-table-column
                                prop="remark"
                                label="备注">
                        </el-table-column>
                        <el-table-column
                                width="200"
                                label="操作">
                            <template scope="scope">
                                <el-button type="primary" size="small" icon="el-icon-edit" @click="showEdit(scope.row)" plain>修改</el-button>
                                <el-button type="danger" size="small" icon="el-icon-delete" @click="deleteMessage(scope.row)" plain>删除</el-button>
                            </template>
                        </el-table-column>
                    </el-table>
                </template>
                <div class="page-block" >
                    <el-pagination
                            @current-change="handleCurrentChange"
                            layout="total,prev, pager, next"
                            :total= page.total
                            :page-size = page.pageSize
                            class="page"
                    >
                    </el-pagination>
                </div>
            </el-row>
        </div>

        <el-dialog :title="isNew ? '新增IP分配信息' : '修改IP分配信息'" :visible.sync="dialogFormVisible">
            <el-form :model="form" ref="form" :rules="rules" label-width="260px" size="small">
                <el-form-item label="网络组：" prop="netid">
                    <el-select v-model="form.netid" placeholder="请选择网络组" size="small" clearable>
                        <el-option v-for="item in netList" :label="item.netname" :value="item.id"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="IP：" prop="ip">
                    <el-input placeholder="请输入IP" v-model="form.ip" size="small" style="width: 280px"></el-input>  如：2a0e:b107:1d9::
                </el-form-item>
                <el-form-item label="子网掩码：" prop="mask">
                    <el-select v-model="form.mask" placeholder="请选择子网掩码" size="small" clearable>
                        <el-option v-for="num in 127" :label="'/' + num" :value="num"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="备注：" prop="remark">
                    <el-input type="textarea" row="3" placeholder="请输入备注" v-model="form.remark" size="small" style="width: 280px"></el-input>
                </el-form-item>
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="dialogFormVisible = false" size="small">取 消</el-button>
                <el-button type="primary" @click="saveEdit" size="small">确 定</el-button>
            </div>
        </el-dialog>

        <el-dialog title="批量新增IP分配信息" :visible.sync="dialogBatchFormVisible">
            <el-form :model="batchForm" ref="batchForm" :rules="batchRules" label-width="260px" size="small">
                <el-form-item label="网络组：" prop="netid">
                    <el-select v-model="batchForm.netid" placeholder="请选择网络组" size="small" clearable>
                        <el-option v-for="item in netList" :label="item.netname" :value="item.id"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="文本记录：" prop="ipTxt">
                    <p>每行一条记录,记录格式为:ip+/+子网掩码</p>
                    <el-input type="textarea" :rows="10" placeholder="每行一条记录,记录格式为:ip+/+子网掩码" v-model="batchForm.ipTxt" size="small" style="width: 360px"></el-input>
                </el-form-item>
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="dialogBatchFormVisible = false" size="small">取 消</el-button>
                <el-button type="primary" @click="saveBatchAdd" size="small">确 定</el-button>
            </div>
        </el-dialog>
    </div>
</div>
<script>
    var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
</script>
<script language="JavaScript" src="js/bk-manage-ipv6.js"></script>
<div id="manage-records-url">
    <div style="padding: 20px;">

        <div style="border: 1px solid #f7f7f7;">
            <el-row>
                <el-form :inline="true" :model="formQuery" class="demo-form-inline">
                    <el-form-item label="域名：">
                        <el-input v-model="formQuery.domain" size="small" style="width: 160px" placeholder="域名" clearable></el-input>
                    </el-form-item>
                    <el-form-item label="会员账号：">
                        <el-input v-model="formQuery.username" size="small" style="width: 160px" placeholder="会员账号" clearable></el-input>
                    </el-form-item>
                    <el-form-item>
                        <el-button type="primary" size="small" @click="onQuery">查询</el-button>
                    </el-form-item>
                </el-form>
            </el-row>
            <el-row>
                <el-button-group>
                    <el-button type="warning" v-loading="oldBtnLoading" size="small" icon="el-icon-right" style="float: left" @click="dealOld">旧数据关联域名记录</el-button>
                    <el-button type="danger" size="small" icon="el-icon-s-check" style="float: left" @click="deleteAll">批量删除</el-button>
                </el-button-group>
                <el-button type="primary" icon="el-icon-circle-plus-outline" style="float: right" size="small" @click="showAdd">新增</el-button>
            </el-row>
            <el-row style="margin-top: 10px">
                <template>
                    <el-table
                            :data="tableData"
                            style="width: 100%"
                            border
                            stripe
                            v-loading="listLoading"
                            size="small"
                            @selection-change="handleSelectionChange">
                        <el-table-column
                                type="selection"
                                width="55">
                        </el-table-column>
                        <el-table-column
                                prop="name"
                                label="主机名">
                        </el-table-column>
                        <el-table-column
                                prop="type"
                                label="转发类型">
                            <template scope="scope">
                                <span v-if="scope.row.URLtype=='0'">显性转发</span>
                                <span v-else>隐性转发</span>
                            </template>
                        </el-table-column>
                        <el-table-column
                                prop="URL"
                                label="转发地址">
                        </el-table-column>
                        <el-table-column
                                prop="origin"
                                label="域名">
                        </el-table-column>
                        <el-table-column
                                prop="title"
                                label="标题">
                        </el-table-column>
                        <el-table-column
                                prop="username"
                                label="所属会员">
                        </el-table-column>
                        <el-table-column
                                width="190"
                                label="操作">
                            <template scope="scope">
                                <el-button type="primary" size="small" icon="el-icon-edit" @click="showEdit(scope.row)" plain>修改</el-button>
                                <el-button type="danger" size="small" icon="el-icon-delete" @click="deleteMessage(scope.row)" plain>删除</el-button>
                            </template>
                        </el-table-column>
                    </el-table>
                </template>
                <div class="page-block" >
                    <el-pagination
                            @current-change="handleCurrentChange"
                            layout="total,prev, pager, next"
                            :total= page.total
                            :page-size = page.pageSize
                            class="page"
                    >
                    </el-pagination>
                </div>
            </el-row>
        </div>

        <el-dialog :title="isNew ? '新增解析记录' : '修改记录'" :visible.sync="dialogFormVisible">
            <el-form :model="form" ref="form" :rules="rules" label-width="260px" size="small">
                <el-form-item label="域名：" prop="origin">
                    <el-input :disabled="!isNew" placeholder="请输入域名" v-model="form.origin" size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="主机名：" prop="name">
                    <el-input placeholder="请输入主机名" v-model="form.name" size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="类型：" prop="URLtype">
                    <el-select v-model="form.URLtype" placeholder="请选择类型" size="small">
                        <el-option v-for="item in typeList" :label="item.label" :value="item.value"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="解析组：" prop="netid">
                    <el-select v-model="form.netid" placeholder="请选择状态" size="small">
                        <el-option v-for="item in netList" :label="item.netname" :value="item.id"></el-option>
                    </el-select>
                </el-form-item>
                <el-form-item label="转发地址：" prop="URL">
                    <el-input type="textarea" placeholder="请输入转发地址" v-model="form.URL" size="small" style="width: 280px"></el-input>
                </el-form-item>
                <el-form-item label="标题：" prop="title">
                    <el-input placeholder="请输入标题" :max="25" v-model="form.title" size="small" style="width: 280px"></el-input>
                </el-form-item>
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="dialogFormVisible = false" size="small">取 消</el-button>
                <el-button type="primary" @click="saveEdit" size="small">确 定</el-button>
            </div>
        </el-dialog>

    </div>
</div>
<script>
    var iFrameToken = '<?php echo \restphp\tpl\RestTpl::get("iFrameToken"); ?>';
</script>
<script language="JavaScript" src="js/bk-manage-records-url.js"></script>